# Roamler

An application to view a list of Roamler submissions.

## Assignment

This is a small project made for the frontend [assignment](Programming Exercise - Location front end.docx) for Roamler.

## Core points

I made a bold decision here. When you start up the application, you'll notice that
none of the sorting, filtering and pagination is functionally working.
This is very much intentional. With the React and redux it would have been very easy to load
the json files and store them in application state and then write a selector that filters and sorts the results.
As a matter of fact, i created a seperate branche for this to show how this would work.  

However, such an approach would never be a production ready solution, because this would always
require offloading of the complete dataset in order to work.
With possibly millions of submissions and thousands of new submissions each day, this would create
lots of problems in real live.

In my opinion, filtering, sorting and paginating of data should always be a backend responsibility.
Having the filtering, sorting and paginating been done on the backend is way more efficient as you can directly query the database.
It also allows for better caching on both backend as on frontend.
I therefore decided to implement a restfull approach to this. However, since this is a front-end assignment,
i implemented a mocked backend. This mocked backend always returns the same result. If you look at your network tab,
you'll see that my solution follows restFull industry standards and could be deployed to production when you
have a working backend.

Also, The backend returns the urls for pagination. This is useful if in a later stage we'd want to implement cursor based pagination.

## The Stack

The application is build using the React-Skeleton from hjeti.
https://github.com/hjeti/react-skeleton
The react skeleton is MIT licenced and it is a very complete base to build production ready react applications.
`React skeleton` comes packaged with a variety of tools for creating a (multilingual) SPA. The
skeleton allows to get up and running in a matter of minutes.

### How to install

The application is based on Yarn, with any version of Node higher than 8 and lower than 13.
* Run 'Yarn' to install dependencies
* Run 'Yarn dev'

After that you should be good to go.

## Todo's

I did not yet add unit-tests.
Should this project grow, it's probably a good idea to subtract more individual components for reusability.

## Time

I spent a bit more time than originally anticipated. All in all, I think I spent about 12 hours.
This was mainly due to the setup of the mock server.
